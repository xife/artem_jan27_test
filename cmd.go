package main

func cmdError(err error) (r1 interface{}) {
	r1 = map[string]interface{}{
		"error":      true,
		"message":    "internal error",
		"sysmessage": err.Error(),
	}
	return r1
}

func cmdDB(f1 LoopFunc, m1 map[string]interface{}) (r1 interface{}) {
	m := InitCallbackMessage(f1, m1)
	var err error
	r1, err = pool.SendMessage(m)
	if err != nil {
		return cmdError(err)
	}
	return r1
}
func cmdLogout(m1 map[string]interface{}) (r1 interface{}) {
	token := ToString(m1["token"])

	sm.Remove(token)

	_, token = sm.GetOrCreate("")

	return map[string]interface{}{
		"ok":     true,
		"page":   "home",
		"reload": true,
		"token":  token,
	}
}
func cmdHello(m1 map[string]interface{}) (r1 interface{}) {
	// create new session
	// token := generateToken()
	// r1 = map[string]interface{}{"token": token}

	// m1 := ToInterfaceMap(m1)
	token := ToString(m1["token"])

	session, token := sm.GetOrCreate(token)

	if session == nil {
		return map[string]interface{}{
			"error":   true,
			"reload":  true,
			"message": "Please reload application",
		}
	}

	r1 = map[string]interface{}{"token": token}

	return r1
}
