package main

import (
	"fmt"
	"strings"
)

func dbEditProfile_updateEmail(c *Connection, session *Session, email string) (r1 interface{}, err error) {
	fmt.Printf("dbEditProfile_updateEmail %s %s %s\n", session.RecordId, toEmailId(email), email)

	stmt1, err := c.db.Prepare("SELECT id, emailid FROM profile WHERE id = ? OR emailid = ?")
	if err != nil {
		return nil, err
	}

	rows, res, err := stmt1.Exec(session.RecordId, toEmailId(email))

	if err != nil {
		return nil, err
	}
	idId := res.Map("id")
	idEmail := res.Map("emailid")
	m1 := map[string]string{}
	for _, row := range rows {
		id := row.Str(idId)
		email := row.Str(idEmail)
		m1[id] = email
		fmt.Printf("id: %s e: %s\n", id, email)
	}
	fmt.Printf("m1: %v\n", m1)
	if len(m1) == 1 {
		fmt.Printf("$1001\n")
		if m1[session.RecordId] == toEmailId(email) {
			fmt.Printf("$1002\n")
			// nothing to change
			return nil, nil
		} else if !strings.HasPrefix(m1[session.RecordId], "email:") {
			fmt.Printf("$1002a\n")
			// may change profileEmail only
			// Will change email address
			fmt.Printf("$1005\n")

			ins, err := c.db.Prepare("UPDATE profile SET profileEmail = ? WHERE id = ?")
			if err != nil {
				fmt.Printf("$1006\n")
				return nil, err
			}

			// salttoken := base64.StdEncoding.EncodeToString(b1)
			_, _, err = ins.Exec(email, session.RecordId)

			if err != nil {
				fmt.Printf("$1007\n")
				return nil, err
			}

			fmt.Printf("$1008\n")
			return nil, nil

		} else {
			fmt.Printf("$1003\n")
			// may change profileEmail and emailid
			// Will change email address
			fmt.Printf("$1005\n")

			ins, err := c.db.Prepare("UPDATE profile SET emailid = ?, profileEmail = ? WHERE id = ?")
			if err != nil {
				fmt.Printf("$1006\n")
				return nil, err
			}

			// salttoken := base64.StdEncoding.EncodeToString(b1)
			_, _, err = ins.Exec(toEmailId(email), email, session.RecordId)

			if err != nil {
				fmt.Printf("$1007\n")
				return nil, err
			}

			fmt.Printf("$1008\n")
			return nil, nil

		}
	} else {
		//
		fmt.Printf("$1004\n")
		return map[string]interface{}{
			"error":   true,
			"message": "You can't use that email address",
		}, nil
	}

}

func dbEditProfile(c *Connection, i1 interface{}) interface{} {
	fmt.Printf("dbEditProfile %v\n", i1)

	m1 := ToInterfaceMap(i1)
	token := ToString(m1["token"])

	session := sm.Get(token)

	if session == nil {
		return map[string]interface{}{
			"error":   true,
			"reload":  true,
			"message": "Please reload application",
		}
	}

	if session.RecordId == "" {
		return map[string]interface{}{
			"error":   true,
			"login":   true,
			"message": "Please login or signup",
			"page":    "login",
		}
	}

	profile := ToInterfaceMap(m1["profile"])

	profile_name := ToString(profile["name"])
	profile_address := ToString(profile["address"])
	profile_email := ToString(profile["email"])
	profile_phone := ToString(profile["phone"])

	if profile_email != "" {
		if !badValidateEmail(profile_email) {
			return map[string]interface{}{
				"error":   true,
				"message": "Email address you entered is not valid",
			}
		}
	}

	r1, err := dbEditProfile_updateEmail(c, session, profile_email)
	if err != nil {
		fmt.Printf("$1009\n")
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #106",
			"sysmessage": err.Error()}
	}
	if r1 != nil {
		fmt.Printf("$1010\n")
		return r1
	}

	fmt.Printf("$1011\n")
	ins, err := c.db.Prepare("UPDATE profile SET name = ?, address = ?, phone = ? WHERE id = ?")

	if err != nil {
		fmt.Printf("$1011\n")
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #107",
			"sysmessage": err.Error()}
	}

	// salttoken := base64.StdEncoding.EncodeToString(b1)
	_, _, err = ins.Exec(profile_name, profile_address, profile_phone, session.RecordId)

	if err != nil {
		fmt.Printf("$1012\n")
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #108",
			"sysmessage": err.Error()}
	}

	// autoid = int(res1.InsertId())

	fmt.Printf("$1013\n")
	return map[string]interface{}{
		"ok":   true,
		"page": "view-profile",
	}
}
