package main

import (
	"fmt"
	"net/url"
	"time"
)

func dbForgot(c *Connection, i1 interface{}) interface{} {
	m1 := ToInterfaceMap(i1)
	email := ToString(m1["email"])
	token := ToString(m1["token"])

	if !badValidateEmail(email) {
		return map[string]interface{}{
			"error":   true,
			"message": "Email address you entered is not valid",
		}
	}

	session := sm.Get(token)

	if session == nil {
		return map[string]interface{}{
			"error":   true,
			"reload":  true,
			"message": "Please reload application",
		}
	}

	// rows, res, err := c.db.Query("SELECT id, password, ptoken FROM profile WHERE emailid = %s", toEmailId(email))

	stmt1, err := c.db.Prepare("SELECT id, password, ptoken FROM profile WHERE emailid = ?")
	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #101.1",
			"sysmessage": err.Error()}
	}

	rows, _, err := stmt1.Exec(toEmailId(email))

	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #101",
			"sysmessage": err.Error()}
	}

	if len(rows) == 0 {
		return map[string]interface{}{
			"error":   true,
			"signup":  true,
			"message": "No such user"}
	}

	// send email to email

	time1 := time.Now().Unix()

	r1 := generateNonVerifiedCSRF1(time1, email, "forgot-password")
	r2 := fmt.Sprintf("%d.%s", time1, r1)

	values := url.Values{}
	values.Add("page", "reset")
	values.Add("email", email)
	values.Add("key", r2)

	encoded := values.Encode()
	url1 := settings.BaseUrl + "#" + encoded

	text1 := "Click following link to Reset Password\n\n" + url1 + "\n\nThanks"

	go sendEmail1(email, "Reset Password", text1)

	return map[string]interface{}{
		"ok":   true,
		"page": "thank-you",
	}
}
