package main

import "fmt"

func dbLinkedinLoginOrSignup(c *Connection, i1 interface{}) interface{} {
	m1 := ToInterfaceMap(i1)
	linkedinId := ToString(m1["id"])
	token := ToString(m1["token"])
	// select is linkedin user with id is already registered

	session := sm.Get(token)

	if session == nil {
		return map[string]interface{}{
			"error":   true,
			"reload":  true,
			"message": "Please reload application",
		}
	}

	stmt1, err := c.db.Prepare("SELECT id FROM profile WHERE emailid = ?")
	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #110.1",
			"sysmessage": err.Error()}
	}

	rows, res, err := stmt1.Exec(toLinkedinId(linkedinId))

	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #110",
			"sysmessage": err.Error()}
	}

	if len(rows) != 0 {
		// START login

		idId := res.Map("id")

		for _, row := range rows {
			id := row.Str(idId)
			session.RecordId = id
			return map[string]interface{}{
				"ok":      true,
				"message": "You are logged in",
				"page":    "view-profile",
			}
		}

		return map[string]interface{}{
			"error":   true,
			"message": "Failed login with LinkedIn",
		}

		// END login
	} else {
		// START signup

		ins, err := c.db.Prepare("INSERT profile SET emailid=?, ctime = NOW()")

		if err != nil {
			c.bad = true
			return map[string]interface{}{
				"error":      true,
				"message":    "database error #110.3",
				"sysmessage": err.Error()}
		}

		_, res1, err := ins.Exec(toLinkedinId(linkedinId))

		if err != nil {
			c.bad = true
			return map[string]interface{}{
				"error":      true,
				"message":    "database error #110.4",
				"sysmessage": err.Error()}
		}

		session.RecordId = fmt.Sprintf("%d", res1.InsertId())

		return map[string]interface{}{
			"ok":      true,
			"message": "You are signed in",
			"page":    "edit-profile",
		}

		// END signup
	}

}
