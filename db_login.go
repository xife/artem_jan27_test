package main

import "encoding/base64"

func dbLogin(c *Connection, i1 interface{}) interface{} {

	m1 := ToInterfaceMap(i1)
	email := ToString(m1["email"])
	token := ToString(m1["token"])
	password := ToString(m1["password"])

	if email != "" {
		if !badValidateEmail(email) {
			return map[string]interface{}{
				"error":   true,
				"message": "Email address you entered is not valid",
			}
		}
	}

	session := sm.Get(token)

	if session == nil {
		return map[string]interface{}{
			"error":   true,
			"reload":  true,
			"message": "Please reload application",
		}
	}

	// rows, res, err := c.db.Query("SELECT id, password, ptoken FROM profile WHERE emailid = %s", toEmailId(email))

	stmt1, err := c.db.Prepare("SELECT id, password, ptoken FROM profile WHERE emailid = ?")
	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #101.1",
			"sysmessage": err.Error()}
	}

	rows, res, err := stmt1.Exec(toEmailId(email))

	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #101",
			"sysmessage": err.Error()}
	}

	if len(rows) == 0 {
		return map[string]interface{}{
			"error":   true,
			"signup":  true,
			"message": "No such user"}
	}

	idId := res.Map("id")
	idPassword := res.Map("password")
	idPToken := res.Map("ptoken")

	for _, row := range rows {
		id := row.Str(idId)
		password1 := row.Str(idPassword)
		ptoken := row.Str(idPToken)

		if ptoken == "" {
			// !!! Linked in signup
			return map[string]interface{}{
				"error":    true,
				"linkedin": true,
				"message":  "linked in sign in",
			}
		}

		bptoken, err := base64.StdEncoding.DecodeString(ptoken)

		if err != nil {
			// !!! Linked in signup
			return map[string]interface{}{
				"error":      true,
				"message":    "database record is corrupted",
				"sysmessage": err.Error(),
			}
		}

		password2 := salt(bptoken, password)

		if password1 == password2 {
			session.RecordId = id
			return map[string]interface{}{
				"ok":      true,
				"message": "You are logged in",
				"page":    "view-profile",
			}
		} else {
			return map[string]interface{}{
				"error":   true,
				"message": "Wrong Password",
			}
		}

	}

	return map[string]interface{}{"ok": true}
}
