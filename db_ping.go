package main

import "fmt"

func dbPing(c *Connection, i1 interface{}) interface{} {
	fmt.Printf("dbPing\n")
	_, _, err := c.db.Query("SELECT 1")

	if err != nil {
		c.bad = true
		return map[string]interface{}{"error": true}
	}

	return map[string]interface{}{"ok": true}
}
