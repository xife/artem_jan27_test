package main

import (
	crand "crypto/rand"
	"encoding/base64"
)

func dbReset(c *Connection, i1 interface{}) interface{} {
	m1 := ToInterfaceMap(i1)
	email := ToString(m1["email"])
	key := ToString(m1["key"])
	token := ToString(m1["token"])
	password := ToString(m1["password"])
	password2 := ToString(m1["password2"])

	if !badValidateEmail(email) {
		return map[string]interface{}{
			"error":   true,
			"message": "Email address you entered is not valid",
		}
	}

	if password != password2 {
		return map[string]interface{}{
			"error":   true,
			"message": "Password and Retype do not match",
		}
	}

	session := sm.Get(token)

	if session == nil {
		return map[string]interface{}{
			"error":   true,
			"reload":  true,
			"message": "Please reload application",
		}
	}

	if verifyNonVerifiedCSRF1(email, key, "forgot-password") {
		// good link reset

		// find id by email

		stmt1, err := c.db.Prepare("SELECT id FROM profile WHERE emailid = ?")
		if err != nil {
			c.bad = true
			return map[string]interface{}{
				"error":      true,
				"message":    "database error #101.1",
				"sysmessage": err.Error()}
		}

		rows, res, err := stmt1.Exec(toEmailId(email))

		if err != nil {
			c.bad = true
			return map[string]interface{}{
				"error":      true,
				"message":    "database error #101",
				"sysmessage": err.Error()}
		}

		if len(rows) == 0 {
			return map[string]interface{}{
				"error":   true,
				"signup":  true,
				"message": "No such user"}
		}

		idId := res.Map("id")

		for _, row := range rows {
			id := row.Str(idId)
			// update password

			ins, err := c.db.Prepare("UPDATE profile SET password=?, ptoken=? WHERE id = ?")

			if err != nil {
				c.bad = true
				return map[string]interface{}{
					"error":      true,
					"message":    "database error #103",
					"sysmessage": err.Error()}
			}

			b1 := make([]byte, 16)
			_, err = crand.Read(b1)
			if err != nil {
				return map[string]interface{}{
					"error":      true,
					"message":    "random error",
					"sysmessage": err.Error()}
			}

			password3 := salt(b1, password)
			salttoken := base64.StdEncoding.EncodeToString(b1)
			_, _, err = ins.Exec(password3, salttoken, id)

			if err != nil {
				c.bad = true
				return map[string]interface{}{
					"error":      true,
					"message":    "database error #104",
					"sysmessage": err.Error()}
			}

			// email new password

			return map[string]interface{}{
				"ok":   true,
				"page": "reset1",
			}
		}
		return map[string]interface{}{
			"error":   true,
			"message": "Should not be shown as len(rows) > 0",
		}

	} else {
		// bad email
		return map[string]interface{}{
			"error":   true,
			"message": "Link has expired",
		}
	}
}
