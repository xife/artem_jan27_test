package main

import (
	crand "crypto/rand"
	"encoding/base64"
	"fmt"
)

func dbSignup(c *Connection, i1 interface{}) interface{} {

	m1 := ToInterfaceMap(i1)
	email := ToString(m1["email"])
	token := ToString(m1["token"])
	password := ToString(m1["password"])
	password2 := ToString(m1["password2"])

	if email != "" {
		if !badValidateEmail(email) {
			return map[string]interface{}{
				"error":   true,
				"message": "Email address you entered is not valid",
			}
		}
	}

	// !!! check email and password length
	// email - 100
	// pass - 50

	session := sm.Get(token)

	if session == nil {
		return map[string]interface{}{
			"error":   true,
			"reload":  true,
			"message": "Please reload application",
		}
	}

	if password != password2 {
		return map[string]interface{}{
			"error":   true,
			"message": "Password and Retype do not match",
		}
	}

	stmt1, err := c.db.Prepare("SELECT password, ptoken FROM profile WHERE emailid = ?")
	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #102.1",
			"sysmessage": err.Error()}
	}

	rows, _, err := stmt1.Exec(toEmailId(email))
	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #102",
			"sysmessage": err.Error()}
	}

	if len(rows) > 0 {
		return map[string]interface{}{
			"error":   true,
			"message": "Email address already registered"}
	}

	ins, err := c.db.Prepare("INSERT profile SET emailid=?, password=?, ptoken=?, profileEmail = ?, ctime = NOW()")

	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #103",
			"sysmessage": err.Error()}
	}

	b1 := make([]byte, 16)
	_, err = crand.Read(b1)
	if err != nil {
		return map[string]interface{}{
			"error":      true,
			"message":    "random error",
			"sysmessage": err.Error()}
	}

	password3 := salt(b1, password)
	salttoken := base64.StdEncoding.EncodeToString(b1)
	_, res1, err := ins.Exec(toEmailId(email), password3, salttoken, email)

	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #104",
			"sysmessage": err.Error()}
	}

	session.RecordId = fmt.Sprintf("%d", res1.InsertId())

	return map[string]interface{}{
		"ok":      true,
		"message": "You are signed in",
		"page":    "edit-profile",
	}
}
