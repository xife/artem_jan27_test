package main

// will return profile as
func dbViewProfile(c *Connection, i1 interface{}) interface{} {

	m1 := ToInterfaceMap(i1)
	token := ToString(m1["token"])

	session := sm.Get(token)

	if session == nil {
		return map[string]interface{}{
			"error":   true,
			"reload":  true,
			"message": "Please reload application",
		}
	}

	if session.RecordId == "" {
		return map[string]interface{}{
			"error":   true,
			"login":   true,
			"message": "Please login or signup",
			"page":    "login",
		}
	}

	// rows, res, err := c.db.Query("SELECT name, address, email, phone FROM profile WHERE id = %s", session.RecordId)

	stmt1, err := c.db.Prepare("SELECT name, address, profileEmail, phone, ctime FROM profile WHERE id = ?")
	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #105.1",
			"sysmessage": err.Error()}
	}

	rows, res, err := stmt1.Exec(session.RecordId)

	if err != nil {
		c.bad = true
		return map[string]interface{}{
			"error":      true,
			"message":    "database error #105",
			"sysmessage": err.Error()}
	}

	if len(rows) == 0 {
		return map[string]interface{}{
			"error":   true,
			"message": "Profile does not exists."}
	}

	idName := res.Map("name")
	idAddress := res.Map("address")
	idEmail := res.Map("profileEmail")
	idPhone := res.Map("phone")
	idCtime := res.Map("ctime")

	for _, row := range rows {
		name := row.Str(idName)
		address := row.Str(idAddress)
		email := row.Str(idEmail)
		phone := row.Str(idPhone)
		ctime := row.Str(idCtime)

		return map[string]interface{}{
			"ok": true,
			"profile": map[string]interface{}{
				"name":    name,
				"address": address,
				"email":   email,
				"phone":   phone,
				"ctime":   ctime,
			},
		}
	}

	return map[string]interface{}{
		"error":   true,
		"message": "DB error. No record",
	}
}
