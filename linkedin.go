package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

// redirects to linkedin service
func handleLinkedinLogin(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("token")
	fmt.Printf("handleLinkedinLogin %v %v\n", cookie, err)
	if err != nil {
		http.Redirect(w, r, settings.BaseUrl+"#page=home", 301)
		return
	}

	token := cookie.Value
	session := sm.Get(token)

	if session == nil {
		fmt.Printf("handleLinkedinLogin session == nil\n")
		http.Redirect(w, r, settings.BaseUrl+"#page=home", 301)
		return
	}

	fakeCSRF := generateNonVerifiedCSRF(token)

	u, err := url.Parse("https://www.linkedin.com/oauth/v2/authorization")
	if err != nil {
		log.Fatal(err)
	}
	q := u.Query()
	q.Set("response_type", "code")
	q.Set("client_id", settings.Linkedin.ClientID)
	q.Set("redirect_uri", settings.BaseUrl+"LinkedInRedirect")
	q.Set("state", fakeCSRF)
	// use basic info only
	// q.Set("scope", "r_basicprofile r_emailaddress")
	q.Set("scope", "r_basicprofile")
	u.RawQuery = q.Encode()
	http.Redirect(w, r, u.String(), 301)
}

// handle linkedin redirect data
// use token stored in cookie.
// cookie is used for linkedin login
func handleLinkedInRedirect(w http.ResponseWriter, r *http.Request) {
	// fmt.Printf("handleLinkedInRedirect %s\n", r.URL.String())

	var token string

	// user_cancelled_login or user_cancelled_authorize
	errmes := r.FormValue("error")
	// fmt.Printf("ERROR: %s\n", errmes)
	if errmes != "" {
		http.Redirect(w, r, settings.BaseUrl+"#page=error-auth", 301)
		return
	}

	code := r.FormValue("code")
	// fmt.Printf("CODE: %s\n", code)
	state := r.FormValue("state")
	// fmt.Printf("STATE: %s\n", state)

	cookie, err := r.Cookie("token")
	if err != nil {
		http.Redirect(w, r, settings.BaseUrl+"/#page=error-auth", 301)
		return
	} else {
		token = cookie.Value
		session := sm.Get(token)

		if session == nil {
			fmt.Printf("SESSION IS NIL\n")
			http.Redirect(w, r, settings.BaseUrl+"/#page=error-auth", 301)
			return
		} else {
		}
		verify := verifyNonVerifiedCSRF(token, state)
		// fmt.Printf("VERIFICATION: %v\n", verify)
		if !verify {

			http.Redirect(w, r, settings.BaseUrl+"/#page=error-auth", 301)
			return
		}
	}

	if code != "" {
		// verified
		liToken, err := linkedinAccessToken(code)
		if err != nil {
			http.Redirect(w, r, settings.BaseUrl+"/#page=error-auth", 301)
			return
		}

		// fmt.Printf("LI TOKEN: %s\n", liToken)

		m1, err := linkedinMyInfo(liToken)
		if err != nil {
			http.Redirect(w, r, settings.BaseUrl+"/#page=error-auth", 301)
			return
		}

		// fmt.Printf("LI INFO: %v\n", m1)

		m1["token"] = token

		m2 := cmdDB(dbLinkedinLoginOrSignup, m1)

		_m2 := ToInterfaceMap(m2)
		// fmt.Printf("LI INFO: %v\n", _m2)

		if ToString(_m2["error"]) == "true" {
			http.Redirect(w, r, settings.BaseUrl+"/#page=error-auth", 301)
			return
		}

		http.Redirect(w, r, settings.BaseUrl+"/#page="+ToString(_m2["page"]), 301)
		return
	}

}

// fetch linkedin user unique ID
func linkedinMyInfo(token string) (m1 map[string]interface{}, err error) {
	url1 := "https://api.linkedin.com/v1/people/~?format=json"

	req, err := http.NewRequest("GET", url1, nil)

	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	fmt.Printf("RESPONSE CODE %d %s\n", resp.StatusCode, resp.Status)

	b2, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	fmt.Printf("DATA %s\n", string(b2))

	err = json.Unmarshal(b2, &m1)
	if err != nil {
		return nil, err
	}

	return m1, nil

}

// fetch Linkedin access_token with CODE
func linkedinAccessToken(code string) (access_token string, err error) {
	fmt.Printf("linkedinAccessToken %s\n", code)
	form := url.Values{}
	form.Add("grant_type", "authorization_code")
	form.Add("code", code)
	form.Add("redirect_uri", settings.BaseUrl+"LinkedInRedirect")
	form.Add("client_id", settings.Linkedin.ClientID)
	form.Add("client_secret", settings.Linkedin.ClientSecret)
	encform := form.Encode()

	fmt.Printf("ENCFORM %s\n", encform)

	// test post later
	// req, err := http.NewRequest("POST", "https://www.linkedin.com/oauth/v2/accessToken", strings.NewReader(encform))
	req, err := http.NewRequest("GET", "https://www.linkedin.com/oauth/v2/accessToken?"+encform, nil)

	if err != nil {
		return "", err
	}
	// req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}

	fmt.Printf("RESPONSE CODE %d %s\n", resp.StatusCode, resp.Status)

	b2, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	fmt.Printf("DATA %s\n", string(b2))

	var m1 map[string]interface{}
	err = json.Unmarshal(b2, &m1)
	if err != nil {
		return "", err
	}
	fmt.Printf("JSON %v\n", m1)
	access_token = ToString(m1["access_token"])
	// expires_in = ToString(m1["expires_in"])
	return access_token, nil
}
