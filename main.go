package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
)

// Setting is configuration data from conf/config.json
type settingsRoot struct {
	BaseUrl  string            `json:"baseUrl"`
	Db       *settingsDB       `json:"db"`
	Email    *settingsEmail    `json:"email"`
	Linkedin *settingsLinkedin `json:"linkedin"`
}

type settingsLinkedin struct {
	ClientID     string `json:"clientID"`
	ClientSecret string `json:"clientSecret"`
}

type settingsDB struct {
	Proto    string `json:"proto"`
	Laddr    string `json:"laddr"`
	Raddr    string `json:"raddr"`
	User     string `json:"user"`
	Password string `json:"passwd"`
	Database string `json:"db"`
}

type settingsEmail struct {
	Type     string `json:"type"`
	Key      string `json:"key"`
	Server   string `json:"server"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

var (
	pool          *Pool
	sm            *SessionManager
	entropy       []byte
	settings      settingsRoot
	passwordChars []rune
)

func init() {
	for ch := 'a'; ch <= 'z'; ch++ {
		passwordChars = append(passwordChars, ch)
	}
	for ch := 'A'; ch <= 'Z'; ch++ {
		passwordChars = append(passwordChars, ch)
	}
	for ch := '0'; ch <= '9'; ch++ {
		passwordChars = append(passwordChars, ch)
	}
}

func main() {
	bjson, err := ioutil.ReadFile("conf/config.json")
	if err != nil {
		log.Println("Log file conf/config.json is required")
		return
	}
	err = json.Unmarshal(bjson, &settings)
	if err != nil {
		log.Println("Log file conf/config.json is not JSON")
		return
	}
	entropy, err = ioutil.ReadFile("conf/entropy")
	if err != nil {
		log.Printf(`Create 'conf/entropy' file with random data
e.g. type in console
head -c 100 </dev/urandom >conf/entropy

I do not write file myself as it will violate Item 4.b Database: mySQL (only)
and I do not wish to store entropy bytes in mySQL.
Code that will create entropy file automatically is commected in code.

`)
		return
		// entropy := make([]byte, 100)
		// _, err := crand.Read(entropy)
		// check(err)
		// ioutil.WriteFile("conf/entropy", entropy, 0600)
	}
	// 5-20 connection pools are most effiecient on servers cheaper $1000
	pool = InitPool(5)
	sm = InitSessionManager()
	http.Handle("/", http.FileServer(http.Dir("www")))
	http.HandleFunc("/api/point1", handlePoint1)
	http.HandleFunc("/LinkedInRedirect", handleLinkedInRedirect)
	http.HandleFunc("/LinkedinLogin", handleLinkedinLogin)

	certfile := "conf/fullchain.pem"
	keyfile := "conf/privkey.pem"

	go http.ListenAndServeTLS(":6565", certfile, keyfile, nil)
	// I have not tested without TLS
	// Google chrome will start showing warning in Chrome 56
	// just in case you do not have certificate for test then
	// you need to adjust port in baseUrl in conf/config.json
	http.ListenAndServe(":6564", nil)
}

// entry point for all API calls
func handlePoint1(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")

	// API calls are POSTing JSON
	if r.Method == "POST" {
		b1, e := ioutil.ReadAll(r.Body)
		if e != nil {
			fmt.Fprintf(w, `{ "error": true }`)
			return
		}
		var m1 map[string]interface{}
		//var m1 map[string]string
		e = json.Unmarshal(b1, &m1)
		if e != nil {
			fmt.Fprintf(w, `{ "error": true }`)
			return
		}
		fmt.Printf("cmd: #%v\n", m1)
		cmd := m1["cmd"]

		var m2 interface{}
		if cmd == "hello" {
			m2 = cmdHello(m1)
		} else if cmd == "login" {
			// cmdDB calls dbLogin function in database pool
			m2 = cmdDB(dbLogin, m1)
		} else if cmd == "logout" {
			m2 = cmdLogout(m1)
		} else if cmd == "signup" {
			m2 = cmdDB(dbSignup, m1)
		} else if cmd == "forgot" {
			m2 = cmdDB(dbForgot, m1)
		} else if cmd == "reset" {
			m2 = cmdDB(dbReset, m1)
		} else if cmd == "view-profile" {
			m2 = cmdDB(dbViewProfile, m1)
		} else if cmd == "edit-profile" {
			m2 = cmdDB(dbEditProfile, m1)
		} else {

		}

		// ensure m2 is not NIL
		if m2 == nil {
			fmt.Fprintf(w, "null")
			return
		}

		// I need that cookie for LinkedIn only
		if _m2, ok := m2.(map[string]interface{}); ok {
			if token, ok := _m2["token"].(string); ok {
				cookie := &http.Cookie{
					Name:  "token",
					Value: token,
					Path:  "/",
				}
				http.SetCookie(w, cookie)
			}
		}

		// print well formated JSON
		b2, _ := json.Marshal(m2)
		var out bytes.Buffer
		json.Indent(&out, b2, "", "\t")
		out.WriteTo(w)
	}
}

// is not used in that project - DELETE
func generatePassword() string {
	var password []rune
	for i := 0; i < 8; i++ {
		ch := passwordChars[rand.Intn(len(passwordChars))]
		password = append(password, ch)
	}
	return string(password)
}
