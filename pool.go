package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

import (
	"github.com/ziutek/mymysql/mysql"
	_ "github.com/ziutek/mymysql/native" // Native engine
)

const (
	_ = iota
	POOL_CONNECTION_ENDED
	POOL_CONNECTION_INC
	POOL_CONNECTION_DEC
)

type Pool struct {
	ch       chan int
	callback chan *CallbackMessage
}
type LoopFunc func(_ *Connection, _ interface{}) interface{}

// CallbackMessage is message that is sent to be executed by mysql connection
// and returned back with callback channel
type CallbackMessage struct {
	f1       LoopFunc
	data     interface{}
	callback chan interface{}
}

// Connection is mysql connection created by pool
type Connection struct {
	db   mysql.Conn
	pool *Pool
	bad  bool
}

// InitCallbackMessage creates CallbackMessage
func InitCallbackMessage(f1 LoopFunc, data interface{}) *CallbackMessage {
	m := &CallbackMessage{
		f1:       f1,
		data:     data,
		callback: make(chan interface{}),
	}
	return m
}

// InitPool initialize pool of mysql connection of certain size
// usually (web app case) mysql performs best with limited number of connections < 2*cpu cores
// usually web app expects response instantly
func InitPool(size int) *Pool {
	pool := &Pool{
		ch:       make(chan int, size*2),
		callback: make(chan *CallbackMessage, 100),
	}
	go pool.loop()
	go func() {
		for i := 0; i < size; i++ {
			pool.InitConnection()
			time.Sleep(time.Second)
		}
	}()
	return pool
}

// main pool loop, handle increase/decrease pool size votes.(not implemented)
// starts a new connection when other dies
// connections are started with random delay 1-2 seconds to avoid instant load on mySQL
func (pool *Pool) loop() {
	for {
		select {
		case i1 := <-pool.ch:
			if i1 == POOL_CONNECTION_ENDED {
				// if some connection ended then start another one
				pool.InitConnection()
			} else if i1 == POOL_CONNECTION_INC {
				// Not implemented
				// vote to increase pool
			} else if i1 == POOL_CONNECTION_DEC {
				// Not implemented
				// vote to decrease pool
			}
		}
	}
}

// SendMessage sends message avoiding getting blocked by full channel
// SendMessage waits for response
// if no response in 500ms then votes to increase number of concurrent connections to mySQL
// if no repsonse in 5500ms returns error.
func (pool *Pool) SendMessage(m *CallbackMessage) (r1 interface{}, err error) {
	// check that channel is less then half filled
	// otherwise do not send message in channel to avoid getting blocked
	if len(pool.callback)*2 > cap(pool.callback) {
		return nil, errors.New("channel is full #1002")
	}
	// sending message to connections in pool
	pool.callback <- m

	// we give 500ms + 5000ms for query to be processed
	timer1 := time.NewTimer(time.Second / 2)
	defer func() {
		timer1.Stop()
	}()
	select {
	case <-timer1.C:
		// query took more then 500ms to process.
		// it is more then we expected
		// ask pool to increase connection pool
		pool.ch <- POOL_CONNECTION_INC
	case r1 = <-m.callback:
		// we are done. most requests will return here
		return r1, nil
	}
	// We will not wait more then 5500ms
	timer2 := time.NewTimer(time.Second * 5)
	defer func() {
		timer2.Stop()
	}()
	select {
	case <-timer2.C:
		// query is not processed in time
		// we will return some response to make app responsive
		return nil, errors.New("timeout #1001")
	case r1 = <-m.callback:
		// delayed response if already notified connection loop that query was slowly processed
		return r1, nil
	}
}

// InitConnection creates a new mySQL connection
// connectes to mySQL with 1-2 second delay
// uses settings.Db settings to connect to mySQL
// it is loaded from conf/config.json
func (pool *Pool) InitConnection() *Connection {
	c := &Connection{
		pool: pool,
	}
	go c.loop()
	return c
}
func (c *Connection) loop() {
	// Connection should not start instantly but with random delay 1-2 seconds
	// If mysql server restarted it will distribute connection time
	time.Sleep(time.Second + time.Duration(rand.Int63n(int64(time.Second))))
	fmt.Printf("Connection.Loop()\n")
	// this ticker is used to keep connection alive.
	// otherwise if app would not used more then 12 hours all connection will die
	tick1 := time.NewTicker(time.Second * 20)
	defer func() {
		fmt.Printf("Connection.Loop() ENDED\n")
		tick1.Stop()
		// notify main loop connection has ended and may be a new connection
		// must be started
		c.pool.ch <- POOL_CONNECTION_ENDED
		c.db.Close()
	}()

	c.db = mysql.New(
		settings.Db.Proto,
		settings.Db.Laddr,
		settings.Db.Raddr,
		settings.Db.User,
		settings.Db.Password,
		settings.Db.Database,
	)
	err := c.db.Connect()
	if err != nil {
		// !!! add error message and may be delay
		fmt.Printf("Connection.Loop() err #10 %v\n", err)
		return
	}

	var time1 int64
	pingCntr := 0
	for !c.bad {
		select {
		case m := <-c.pool.callback:
			// requests from our app
			m.callback <- m.f1(c, m.data)
			time1 = time.Now().UnixNano()
			pingCntr = 0
		case <-tick1.C:
			// keep alive ticks come every 20 seconds
			time2 := time.Now().UnixNano()
			if time2-time1 > int64(time.Second)*120 {
				// if no queries was on connection for 2 minutes
				// then ping mysql
				dbPing(c, nil)
				time1 = time2
				pingCntr++
				// if 3 ping happens on connection then
				// notify main loop to decrease number of concurrent connections
				if pingCntr > 3 {
					c.pool.ch <- POOL_CONNECTION_DEC
				}
			}
		}
	}
}
