package main

import (
	"sync"
	"time"
)

// Session stores user data
type Session struct {
	RecordId string // same ID as in mySQL.profile.id
	time1    int64  // last time session was touched, sessions get deleted after 1 hour of inactivity
}

// stores all sessions
type SessionManager struct {
	lck      sync.Mutex
	sessions map[string]*Session
}

func InitSessionManager() *SessionManager {
	sm := &SessionManager{
		sessions: make(map[string]*Session),
	}
	return sm
}
func (sm *SessionManager) Remove(token string) {
	sm.lck.Lock()
	defer sm.lck.Unlock()
	delete(sm.sessions, token)
}
func (sm *SessionManager) clean(time1 int64) {
	for token, session := range sm.sessions {
		// sessions will expire in 1 hour
		if time1-session.time1 > int64(time.Hour) {
			delete(sm.sessions, token)
		}
	}
}
func (sm *SessionManager) GetOrCreate(token string) (session1 *Session, rtoken string) {
	sm.lck.Lock()
	defer sm.lck.Unlock()
	time1 := time.Now().UnixNano()
	sm.clean(time1)
	if session, ok := sm.sessions[token]; ok {
		session.time1 = time1
		return session, token
	} else {
		rtoken := generateToken()
		session = &Session{}
		sm.sessions[rtoken] = session
		session.time1 = time1
		return session, rtoken
	}
}
func (sm *SessionManager) Get(token string) *Session {
	sm.lck.Lock()
	defer sm.lck.Unlock()
	time1 := time.Now().UnixNano()
	sm.clean(time1)
	if session, ok := sm.sessions[token]; ok {
		session.time1 = time1
		return session
	} else {
		// will return nil
		return session
	}
}
