package main

import (
	"bytes"
	crand "crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/smtp"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// sendEmail1 sends email via SMTP/Gmail or MailGun
func sendEmail1(email, subject, body string) error {
	fmt.Printf("SendEmail1 %v\n", settings.Email)
	if settings.Email.Type == "mg" {
		return sendEmailMG(email, subject, body)
	} else {
		return sendEmailNonSecure(email, subject, body)
	}
}

// sendEmailMG sends via MailGun
func sendEmailMG(email, subject, body string) error {
	form := url.Values{}
	form.Add("to", email)
	form.Add("from", settings.Email.Email)
	form.Add("subject", subject)
	form.Add("text", body)

	encform := form.Encode()

	fmt.Printf("ENCFORM %s\n", encform)

	req, err := http.NewRequest("POST", "https://api.mailgun.net/v3/mg.roolez.com/messages", strings.NewReader(encform))

	req.SetBasicAuth("api", settings.Email.Key)

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	fmt.Printf("RESPONSE CODE %d %s\n", resp.StatusCode, resp.Status)

	b2, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	fmt.Printf("DATA %s\n", string(b2))

	var m1 map[string]interface{}
	err = json.Unmarshal(b2, &m1)
	if err != nil {
		return err
	}
	fmt.Printf("JSON %v\n", m1)

	return nil
}

// sends email via SMTP.
// It does not validates fields
// emails in db are already validated
func sendEmailNonSecure(email, subject, body string) error {
	fmt.Printf("SendEmailNonSecure\n")
	if strings.ContainsAny(subject, "\r\n\t") {
		return errors.New("subject line contains not allowed character")
	}
	if !badValidateEmail(email) {
		return errors.New("email not validated")
	}
	text1 := "Subject: " + subject + "\n\n" + body

	ss := strings.Split(settings.Email.Server, ":")
	host := ss[0]

	err := smtp.SendMail(settings.Email.Server,
		smtp.PlainAuth("", settings.Email.Email, settings.Email.Password, host),
		settings.Email.Email, []string{email}, []byte(text1))

	fmt.Printf("email: %s\n", email)
	fmt.Printf("text1: %s\n", text1)
	fmt.Printf("err: %v\n", err)

	return err
}

// check err and panic
func check(err error) {
	if err != nil {
		panic(err)
	}
}

// genarates random token with crypto/rand
func generateToken() string {
	b1 := make([]byte, 16)
	_, err := crand.Read(b1)
	check(err)
	// var buf bytes.Buffer
	token := base64.StdEncoding.EncodeToString(b1)
	return token
}

func salt(token []byte, password string) string {
	var buf bytes.Buffer
	buf.Write(token)
	buf.Write(entropy)
	buf.WriteString(password)
	b2 := sha1.Sum(buf.Bytes())
	r1 := base64.StdEncoding.EncodeToString(b2[:])
	return r1
}

// unique id from email
func toEmailId(email string) string {
	return "email:" + email
}

// unique id from linkedin id
func toLinkedinId(s1 string) string {
	return "linkedin:" + s1
}

// this is something I would not write myself but will use ready code from CSRF or JWT
// creates string from token and time and could be verify later
// NonVerified - means no audit done. Just I wrote it and nobody tested
// could be a source of mistakes
func generateNonVerifiedCSRF(token string) string {
	time1 := time.Now().Unix()
	r1 := generateNonVerifiedCSRF1(time1, token, "generateNonVerifiedCSRF1")
	r2 := fmt.Sprintf("%d.%s", time1, r1)
	return r2
}

// this is something I would not write myself but will use ready code from CSRF ow JWT
// creates string from token and time and could be verify later
// NonVerified - means no audit done. Just I wrote it and nobody tested
// could be a source of mistakes
func generateNonVerifiedCSRF1(time1 int64, token, name string) string {
	var buf bytes.Buffer
	binary.Write(&buf, binary.LittleEndian, time1)
	buf.WriteString(token)
	buf.Write(entropy)
	buf.WriteString(name)
	b1 := buf.Bytes()
	b2 := sha1.Sum(b1)
	r1 := base64.StdEncoding.EncodeToString(b2[:])
	return r1
}

// this is something I would not write myself but will use ready code from CSRF ow JWT
// creates string from token and time and could be verify later
// NonVerified - means no audit done. Just I wrote it and nobody tested
// could be a source of mistakes
func verifyNonVerifiedCSRF(token, state string) bool {
	return verifyNonVerifiedCSRF1(token, state, "generateNonVerifiedCSRF1")
}

// this is something I would not write myself but will use ready code from CSRF ow JWT
// creates string from token and time and could be verify later
// NonVerified - means no audit done. Just I wrote it and nobody tested
// could be a source of mistakes
func verifyNonVerifiedCSRF1(token, state, name string) bool {
	fmt.Printf("verifyNonVerifiedCSRF %s %s\n", token, state)
	pos := strings.Index(state, ".")
	if pos == -1 {
		fmt.Printf("@1\n")
		return false
	}
	stime := state[:pos]
	enc := state[pos+1:]
	fmt.Printf("# %s %s\n", stime, enc)

	time1 := time.Now().Unix()
	time2, err := strconv.ParseInt(stime, 10, 64)
	if err != nil {
		fmt.Printf("@2\n")
		return false
	}
	fmt.Printf("### %d %d %d\n", time1, time2, time1-time2)
	if time1-time2 > 3600 {
		return false
	}
	enc2 := generateNonVerifiedCSRF1(time2, token, name)
	fmt.Printf("#! %s %s\n", enc, enc2)
	return enc == enc2
}

// badValidateEmail is validateEmail but not the best one.
// The aim of that function to check that no new line is entered
// if user is allowed to enter new line in email then it is possible to add fake email headers
// or body to email
func badValidateEmail(email string) bool {
	Re := regexp.MustCompile(`^[a-z0-9\._%+\-]+@[a-z0-9\.\-]+$`)
	return Re.MatchString(email)
}
