function poster(url, o1, f1) {

  xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 200) {
        s1 = xhr.responseText
        console.log('got', s1)
        var json = JSON.parse(s1);
        if (f1 != null) {
          f1(json)
        }
      }
    }
    // o0 = {"u":url, "o1":o1, "auth": document.getElementById("auth").value}
    // o0 = {"u":url, "o1":o1}
  console.log('sending', o1)
  xhr.send(JSON.stringify(o1));
}

function log(x) {
  console.log('log', x)
}
// parses urls hash
function getHashParams() {
  var hashParams = {};
  var e,
    a = /\+/g, // Regex for replacing addition symbol with a space
    r = /([^&;=]+)=?([^&;]*)/g,
    d = function(s) {
      return decodeURIComponent(s.replace(a, " "));
    },
    q = location.hash.substring(1);

  while (e = r.exec(q))
    hashParams[d(e[1])] = d(e[2]);

  return hashParams;
}
window.onhashchange = hashchange1;
window.onload = windowLoad;

function windowLoad() {
  submitHello()
}
// called when hash get changed
function hashchange1() {
  process1()
}

function process1() {
  var hsh = getHashParams();
  if ("page" in hsh) {
    openPage(hsh.page)
  }
}
// called when page param of hash has changed
function openPage(pageId) {
  var pagesDiv = document.getElementById("pages")
  var pagesDivChildren = pagesDiv.children
  if (pageId == "") {
    pageId = "home"
  }
  for (var i = 0; i < pagesDivChildren.length; i++) {
    var childDiv = pagesDivChildren[i]
    if (childDiv.id == "page-" + pageId) {
      childDiv.style.display = "block"
    } else {
      childDiv.style.display = "none"
    }
  }
  if (pageId == "view-profile") {
    loadProfile()
  }
  if (pageId == "edit-profile") {
    loadProfile()
  }
  if (pageId == "logout") {
    submitLogout()
  }
}
// changes page's hash
function goPage(pageId) {
  document.location.hash = "page=" + pageId
}

// ----
var token = "na"
  // store token in localStorage
  // used to track sessions
function updateToken(x) {
  if ("token" in x) {
    token = x.token
    localStorage.setItem("token", token)
  }
  if ("reload" in x) {
    localStorage.setItem("reload", "home")
    if ("page" in x) {
      localStorage.setItem("reload", x.page)
    }
		var input = document.getElementsByTagName("input");
		input = Array.prototype.slice.call(input)
		input.forEach(function(x){ x.value = '' })
    location.reload()
  }
}

function updatePage(x) {
  if ("page" in x) {
    goPage(x.page)
  }
}
// gets token when loads page
function submitHello() {
  if (token == "na") {
    token = localStorage.getItem("token")
  }
  if (token == null) {
    token = "na"
  }
  var o1 = {
    "cmd": "hello",
    "token": token
  }
  poster("/api/point1", o1, submitHello1)
}

function submitHello1(x) {
  updateToken(x)
  process1()
  updatePage(x)
  page = localStorage.getItem("reload")
  if (page != null) {
    localStorage.removeItem("reload")
    goPage(page)
  }
}

function submitLogin() {
  var o1 = {
    "cmd": "login",
    "token": token
  }
  o1.email = document.getElementById("login-email").value
  o1.password = document.getElementById("login-password").value
  poster("/api/point1", o1, submitLogin1)
  document.getElementById("login-message").style.display = 'none'
}

function submitLogin1(x) {
  updateToken(x)
  updatePage(x)
  if ("error" in x) {
    document.getElementById("login-message").style.display = 'block'
    document.getElementById("login-message").innerText = x.message
  }
}

function submitSignup() {
  var o1 = {
    "cmd": "signup",
    "token": token
  }
  o1.email = document.getElementById("signup-email").value
  o1.password = document.getElementById("signup-password").value
  o1.password2 = document.getElementById("signup-password2").value
  poster("/api/point1", o1, submitSignup1)
  document.getElementById("signup-message").style.display = 'none'
}

function submitSignup1(x) {
  updateToken(x)
  updatePage(x)
  if ("error" in x) {
    document.getElementById("signup-message").style.display = 'block'
    document.getElementById("signup-message").innerText = x.message
  }

}

function submitLogout() {
  var o1 = {
    "cmd": "logout",
    "token": token
  }
  poster("/api/point1", o1, submitLogin1)
}

function submitLogout1(x) {
  updateToken(x)
  updatePage(x)
}

function loadProfile() {
  var o1 = {
    "cmd": "view-profile",
    "token": token
  }
  poster("/api/point1", o1, loadProfile1)
}

function loadProfile1(x) {
  updateToken(x)
  updatePage(x)
  if ("ok" in x) {
    document.getElementById("view-profile-name").innerText = x.profile.name
    document.getElementById("view-profile-email").innerText = x.profile.email
    document.getElementById("view-profile-address").innerText = x.profile.address
    document.getElementById("view-profile-phone").innerText = x.profile.phone
    document.getElementById("view-profile-ctime").innerText = x.profile.ctime

    document.getElementById("edit-profile-name").value = x.profile.name
    document.getElementById("edit-profile-email").value = x.profile.email
    document.getElementById("edit-profile-address").value = x.profile.address
    document.getElementById("edit-profile-phone").value = x.profile.phone
  }
}

function submitProfile() {
  var profile = {}
  var o1 = {
    "cmd": "edit-profile",
    "token": token,
    "profile": profile
  }
  profile.name = document.getElementById("edit-profile-name").value
  profile.email = document.getElementById("edit-profile-email").value
  profile.address = document.getElementById("edit-profile-address").value
  profile.phone = document.getElementById("edit-profile-phone").value
  poster("/api/point1", o1, submitProfile1)
  document.getElementById("edit-profile-message").style.display = 'none'
}

function submitProfile1(x) {
  updateToken(x)
  updatePage(x)
  if ("error" in x) {
    document.getElementById("edit-profile-message").style.display = 'block'
    document.getElementById("edit-profile-message").innerText = x.message
  }
}

function loginLinkedin() {
  location.href = "/LinkedinLogin?r=" + ("" + Math.random()).substring(2)
}

function submitForgot() {
  var o1 = {
    "cmd": "forgot",
    "token": token
  }
  o1.email = document.getElementById("forgot-email").value
  poster("/api/point1", o1, submitForgot1)
  document.getElementById("forgot-message").style.display = 'none'
}

function submitForgot1(x) {
  updateToken(x)
  updatePage(x)
  if ("error" in x) {
    document.getElementById("forgot-message").style.display = 'block'
    document.getElementById("forgot-message").innerText = x.message
  }
  if ("ok" in x) {
    goPage("thank-you")
  }
}

function submitReset() {
  var hsh = getHashParams();
  var o1 = {
    "cmd": "reset",
    "token": token
  }
  o1.email = hsh.email
  o1.key = hsh.key
  o1.password = document.getElementById("reset-password").value
  o1.password2 = document.getElementById("reset-password2").value
  poster("/api/point1", o1, submitReset1)
  document.getElementById("reset-message").style.display = 'none'
}

function submitReset1(x) {
  updateToken(x)
  updatePage(x)
  if ("error" in x) {
    document.getElementById("reset-message").style.display = 'block'
    document.getElementById("reset-message").innerText = x.message
  }
  if ("ok" in x) {
    goPage("reset1")
  }
}
